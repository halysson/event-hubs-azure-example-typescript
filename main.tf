provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-example" {
  name = "rg-example"
  location = "brazilsouth"
}

resource "azurerm_eventhub_namespace" "ns-example" {
  resource_group_name = azurerm_resource_group.rg-example.name
  depends_on = [
    azurerm_resource_group.rg-example
  ]
  name = "ns-example"
  location = "brazilsouth"
  sku = "Standard"
}

resource "azurerm_eventhub" "eh-example" {
  resource_group_name = azurerm_resource_group.rg-example.name
  namespace_name = azurerm_eventhub_namespace.ns-example.name
  depends_on = [
    azurerm_resource_group.rg-example,
    azurerm_eventhub_namespace.ns-example
  ]
  name = "eh-example"
  partition_count = 4
  message_retention = 7
}

resource "azurerm_eventhub_authorization_rule" "ar-example-send" {
  resource_group_name = azurerm_resource_group.rg-example.name
  namespace_name = azurerm_eventhub_namespace.ns-example.name
  eventhub_name = azurerm_eventhub.eh-example.name
  depends_on = [
    azurerm_resource_group.rg-example,
    azurerm_eventhub_namespace.ns-example,
    azurerm_eventhub.eh-example
  ]
  name = "ar-example"
  listen = true
  send = true
  manage = true
}

output "ar-example-send-EVENTHUB_CONNECTION_STRING_PRIMARY" {
  value = azurerm_eventhub_authorization_rule.ar-example-send.primary_connection_string
  depends_on = [
    azurerm_eventhub_authorization_rule.ar-example-send
  ]
  sensitive = true
}

